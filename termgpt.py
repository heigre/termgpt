import openai

openai.api_key = "YOUR-API-KEY"

try:
    while True:
        prompt = input("Question: ")

        completions = openai.Completion.create(
            engine="text-davinci-002",
            prompt=prompt,
            max_tokens=1024,
            n=1,
            stop=None,
            temperature=0.5,
        )

        message = completions.choices[0].text
        print(message)
except KeyboardInterrupt:
    print("Exiting...")
